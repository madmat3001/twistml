r"""<package summary>

    <extended summary>

    <module listings>

:Author:
    Matthias Manhertz
:Copyright:
    (c) Matthias Manhertz 2015
:Licence:
    MIT
"""

# flake8: noqa

from .utility import generate_datesequence
from .utility import setup_logger
from .utility import find_files
from .utility import progress_report
from .utility import query_yes_no
from .utility import remap
from .utility import float_ceil, float_floor
from .toydata import create_toy_data
from .plotting import multi_group_bar_chart