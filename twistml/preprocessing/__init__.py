r"""<package summary>

    <extended summary>

    <module listings>

:Author:
    Matthias Manhertz
:Copyright:
    (c) Matthias Manhertz 2015
:Licence:
    MIT
"""

# flake8: noqa

from .preprocessing import preprocess_tweets