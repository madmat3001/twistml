#!/bin/sh

git stash -q --keep-index

# Test prospective commit
nosetests
result=$?

git stash pop -q

[ $result -ne 0 ] && exit 1
exit 0

